import picamera
import time
import datetime as dt
import threading
from pyvmu import vmu931, messages
from gps3 import gps3
import logging
import csv
import uuid
import os


# set flag for logging data to text file
logFlag = False

# log file count
logFileCount = 1

# create unique filename for text file
unique_file = str(uuid.uuid4())
current_log = ''

# function to write a line to a specified log file
def writeLine(path, data):
    with open(path, 'a') as csv_file:
        writer = csv.writer(csv_file, delimiter = ',')
        writer.writerow(data)

# create a log file ready to append data
def newLog(count):
    file_name = './FDM_log_dt_' + unique_file + '_' + str(count) + '.csv'
    return file_name


class vmu_poller(threading.Thread):

    def __init__(self, device="/dev/ttyACM0", euler=True, accelerometer=False, quaternion=False, heading=True):
        threading.Thread.__init__(self)
        self.__device = device
        self.__euler = euler
        self.__accelerometer = accelerometer
        self.__quaternion = quaternion
        self.__heading = heading


    def run(self):
        global heading
        global yaw
        global pitch
        global roll

#        print("thread started") #test print to see that we are in the thread

        with vmu931.VMU931Parser(device=self.__device, quaternion=self.__quaternion, euler=self.__euler, accelerometer=self.__accelerometer, heading=self.__heading) as vp:

            heading = 1  #initialize heading because it will likely get missed the first time through loop
            ts = 1  #initialize ts because it will possibly get missed the first time through loop
            yaw = 1  #initialize yaw because it will possibly get missed the first time through loop
            pitch = 1  #initialize pitch because it will possibly get missed the first time through loop
            roll = 1  #initialize roll because it will possibly get missed the first time through loop

            while True:

                p = vp.parse()

                if isinstance(p, messages.Euler): # need to verify that these are the correct assignments...
                    ts, x, y, z = p
                    yaw = z          # for forward motion along +'ve x-axis and
                    pitch = y        # according to legend on VMU931
                    roll = x         # these are the assignments
                                     # this will need to be re-verified for final
                                     # installed orientation
                    timestamp = ts

                elif isinstance(p, messages.Heading):
                    ts, x = p
                    heading = x

                elif isinstance(p, messages.Accelerometer):
                    ts, x, y, z = p

                elif isinstance(p, messages.Quaternion):
                    ts, w, x, y, z = p

#                print(dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),heading)




# GPS thread
class gps_poller(threading.Thread):
    def run(self):

        # Bring globals into Scope
        global position
        global logFlag
        global current_log
        global heading
        global yaw
        global pitch
        global roll
        # Set count
        count = 1

        # Get Data from socket
        gps_socket = gps3.GPSDSocket()
        data_stream = gps3.DataStream()
        gps_socket.connect()
        gps_socket.watch()

        # Parse new data in stream
        for new_data in gps_socket:
            if new_data:
                data_stream.unpack(new_data)
                position = data_stream.TPV

                # if logging is true, log to file
#                if logFlag == True:
#                    line = [position['lat'], position['lon'], position['time'], yaw, pitch, roll, heading]
#                    writeLine(current_log, line)

                # Clear the terminal before writing new data.
                os.system('clear')

                # Print new position infor
                print ( 'Lat:       ' + str( position['lat'] ) )
                print ( 'Lng:       ' + str( position['lon'] ) )
                print ( 'Time:      ' + str( position['time'] ) )
                print ( 'Track:     ' + str( position['track'] ) )
                print ( 'Fix:       ' + str( position['mode'] ) )
#                print ( 'heading:       ' + str( heading ))

            # How often to update gps file. 1 = 1 second
            time.sleep(1)
            count += 1




class video_recorder(threading.Thread):

    def run(self):
        m_resolutionX = 1280
        m_resolutionY = 720
        m_FPS = 30
        with picamera.PiCamera() as camera:
            camera.resolution = (m_resolutionX, m_resolutionY)
            camera.framerate = m_FPS
            camera.brightness = 60
            camera.vflip = False
            camera.hflip = False
            camera.annotate_background = picamera.Color('black')

#            camera.start_preview(fullscreen=False, window=(35, 40, 306, 228))
            camera.start_preview(fullscreen=True)
#            camera.annotate_text = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

            camera.start_recording("FDM_tmp.h264") # record to temporary file
            while True:
                camera.annotate_text = str(yaw)+' '+str(heading)
#                camera.annotate_text = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                camera.wait_recording(0.2)

#            camera.stop_recording() # stop the recording

#            camera.stop_preview()

def main():
    # start vmu poller
    vmu_poller().start()

    # start gps poller
    gps_poller().start()


    # wait for the user input to start recording
    input("Press <ENTER> to start recording")
    #start video recorder
    video_recorder().start()

    # enable logging
    logFlag = True
    current_log = newLog(logFileCount)
    print(current_log, 'open for logging')

    while True: 
        if logFlag == True:
            line = [dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), yaw, pitch, roll, heading]
            print(dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            writeLine(current_log, line)
            time.sleep(1)


# Python bit to figure out how who started what
if __name__ == "__main__":
    main()

import picamera
import time
import datetime as dt
import threading
from pyvmu import vmu931, messages
from gps3 import gps3
import logging
import csv
import uuid
import os
import RPi.GPIO as GPIO

GPIO.setwarnings(False)

greenLED1 = 26
greenLED2 = 19
greenLED3 = 13
redLED4 = 16
redLED5 = 20
redLED6 = 21

camera_LED = greenLED1
gps_LED = greenLED2
imu_LED = greenLED3
Accel_alarm = redLED4
Attitude_Alarm = redLED5

blink_me = 0.5



def blinkLED(LED,hold):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(LED, GPIO.OUT)
    count = 0
    while True:
#    while count < 10:
        GPIO.output(LED, True)
        time.sleep(hold)
        GPIO.output(LED, False)
        print(count)
        time.sleep(hold)
        count += 1

def turnON_LED(LED):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(LED, GPIO.OUT)
    GPIO.output(LED, True)
    
def turn_OFF(LED):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(LED, GPIO.OUT)
    GPIO.output(LED, False)

def main():
    print("running")
    blinkLED(camera_LED,blink_me)
    blinkLED(gps_LED,blink_me)
#    turnON_LED(camera_LED)
#    turnON_LED(gps_LED)
#    turnON_LED(imu_LED)
#    GPIO.cleanup()


# Python bit to figure out how who started what
if __name__ == "__main__":
    main()

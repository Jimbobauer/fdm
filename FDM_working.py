#! /usr/bin/env python

import picamera
import time
import datetime as dt
import threading
from pyvmu import vmu931, messages
from gps3 import gps3
import logging
import csv
import uuid
import os
from subprocess import call
import RPi.GPIO as GPIO

greenLED1 = 26
greenLED2 = 19
greenLED3 = 13
redLED4 = 16
redLED5 = 20
redLED6 = 21

camera_LED = greenLED1
gps_LED = greenLED2
imu_LED = greenLED3
Accel_Alarm = redLED4
Attitude_Alarm = redLED5
Spare_LED = redLED6

### initialize GPIOs
def initialize_LEDs():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(camera_LED, GPIO.OUT)
    GPIO.setup(gps_LED, GPIO.OUT)
    GPIO.setup(imu_LED, GPIO.OUT)
    GPIO.setup(Accel_Alarm, GPIO.OUT)
    GPIO.setup(Attitude_Alarm, GPIO.OUT)
    GPIO.setup(Spare_LED, GPIO.OUT)

def turnON_LED(LED):
    GPIO.output(LED, True)

def turnOFF_LED(LED):
    GPIO.output(LED, False)

def turnOFF_ALL_LEDs():
    turnOFF_LED(camera_LED)
    turnOFF_LED(gps_LED)
    turnOFF_LED(imu_LED)
    turnOFF_LED(Accel_Alarm)
    turnOFF_LED(Attitude_Alarm)
    turnOFF_LED(Spare_LED)

def turnON_ALL_LEDs():
    turnON_LED(camera_LED)
    turnON_LED(gps_LED)
    turnON_LED(imu_LED)
    turnON_LED(Accel_Alarm)
    turnON_LED(Attitude_Alarm)
    turnON_LED(Spare_LED)

def flash_LED(LED, hold):
    GPIO.output(LED, True)
    time.sleep(hold)
    GPIO.output(LED, False)


# function to raise an alarm when something happens
def alarm():
    print("ALARM!!")
    flash_LED(Attitude_Alarm, 0.1)


# set flag for logging data to text file
logFlag = False

# log file count
logFileCount = 1

# create unique filename for text file
unique_file = str(uuid.uuid4())
current_log = ''

# function to write a line to a specified log file
def writeLine(path, data):
    with open(path, 'a') as csv_file:
        writer = csv.writer(csv_file, delimiter = ',')
        writer.writerow(data)

# create a log file ready to append data
def newLog(count):
    file_name = '/home/pi/repos/fdm/FDM_log_dt_' + unique_file + '_' + str(count) + '.csv'
    return file_name


class vmu_poller(threading.Thread):

    def __init__(self, device="/dev/ttyACM0", euler=True, accelerometer=True, quaternion=False, heading=True):
        threading.Thread.__init__(self)
        self.__device = device
        self.__euler = euler
        self.__accelerometer = accelerometer
        self.__quaternion = quaternion
        self.__heading = heading


    def run(self):
        global heading
        global yaw
        global pitch
        global roll
        global AccX
        global AccY
        global AccZ

#        print("imu thread started") #test print to see that we are in the thread
        turnON_LED(imu_LED)

        with vmu931.VMU931Parser(device=self.__device, quaternion=self.__quaternion, euler=self.__euler, accelerometer=self.__accelerometer, heading=self.__heading) as vp:

            heading = 1  #initialize heading because it will likely get missed the first time through loop
            ts = 1  #initialize ts because it will possibly get missed the first time through loop
            yaw = 1  #initialize yaw because it will possibly get missed the first time through loop
            pitch = 1  #initialize pitch because it will possibly get missed the first time through loop
            roll = 1  #initialize roll because it will possibly get missed the first time through loop
            AccX = 1  #initialize x-acceleration because it will possibly get missed the first time through loop
            AccY = 1  #initialize y-acceleration because it will possibly get missed the first time through loop
            AccZ = 1  #initialize z-acceleration because it will possibly get missed the first time through loop
            while True:

                p = vp.parse()

                if isinstance(p, messages.Euler): # need to verify that these are the correct assignments...
                    ts, x, y, z = p
                    yaw = z          # for forward motion along +'ve x-axis and
                    pitch = y        # according to legend on VMU931
                    roll = x         # these are the assignments
                                     # this will need to be re-verified for final
                                     # installed orientation
                    timestamp = ts
#                    print("Eulers:", yaw, pitch, roll)

                elif isinstance(p, messages.Heading):
                    ts, x = p
                    heading = x

                elif isinstance(p, messages.Accelerometer):
                    ts, x, y, z = p
                    AccX = x
                    AccY = y
                    AccZ = z
#                    print("Accelerations:", AccX, AccY, AccZ)

                elif isinstance(p, messages.Quaternion):
                    ts, w, x, y, z = p

#                print(dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),heading)




# GPS thread
class gps_poller(threading.Thread):
    def run(self):

        # Bring globals into Scope
        global position
        global logFlag
        global current_log
        global heading
        global yaw
        global pitch
        global roll
        # Set count
        count = 1
        heading = 0
        yaw = 0
        pitch = 0
        roll = 0

#        print("gps thread started") #test print to see that we are in the thread
        turnON_LED(gps_LED)

        # Get Data from socket
        gps_socket = gps3.GPSDSocket()
        data_stream = gps3.DataStream()
        gps_socket.connect()
        gps_socket.watch()

        # Parse new data in stream
        for new_data in gps_socket:
            if new_data:
                data_stream.unpack(new_data)
                position = data_stream.TPV

                # Clear the terminal before writing new data.
                os.system('clear')

                # Print new position info
                print ( 'Lat:       ' + str( position['lat'] ) )
                print ( 'Lng:       ' + str( position['lon'] ) )
                print ( 'Time:      ' + str( position['time'] ) )
                print ( 'Track:     ' + str( position['track'] ) )
                print ( 'Fix:       ' + str( position['mode'] ) )
                print ( 'Yaw:       ' + str( yaw ))
                print ( 'Pitch:       ' + str( pitch ))
                print ( 'Roll:       ' + str( roll ))
                print ( 'Heading:       ' + str( heading ))
                print ( 'Accel_X:       ' + str( AccX ))
                print ( 'Accel_Y:       ' + str( AccY ))
                print ( 'Accel_Z:       ' + str( AccZ ))

            # How often to update gps file. 1 = 1 second
            time.sleep(1)
            count += 1




class video_recorder(threading.Thread):

    def run(self):
        m_resolutionX = 1280
        m_resolutionY = 720
        m_FPS = 15

        turnON_LED(camera_LED)

        with picamera.PiCamera() as camera:
            camera.resolution = (m_resolutionX, m_resolutionY)
            camera.framerate = m_FPS
            camera.brightness = 60
            camera.vflip = False
            camera.hflip = False
            camera.rotation = 270
            camera.annotate_background = picamera.Color('black')

#            camera.start_preview(fullscreen=False, window=(35, 40, 306, 228))
#            camera.start_preview(fullscreen=True)
#            camera.annotate_text = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

            camera.start_recording("/home/pi/repos/fdm/FDM_tmp.h264") # record to temporary file
            while True:

                camera.annotate_text = str( position['time'])

#                camera.annotate_text = str(yaw)+' '+str(heading)
#                camera.annotate_text = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                camera.wait_recording(0.2)

#            camera.stop_recording() # stop the recording

#            camera.stop_preview()

def main():

    time.sleep(1)
    # start the GPS daemon
    call(["sudo","gpsd","/dev/ttyUSB0"," -F"," /var/run/gpsd.sock"])
    time.sleep(0.5)

#    GPIO.cleanup()
    initialize_LEDs()

    turnOFF_ALL_LEDs()
    time.sleep(0.2)
    turnON_ALL_LEDs()
    time.sleep(0.2)
    turnOFF_ALL_LEDs()
    time.sleep(0.2)
    turnON_ALL_LEDs()
    time.sleep(0.2)
    turnOFF_ALL_LEDs()
    time.sleep(0.2)
    turnOFF_ALL_LEDs()
    time.sleep(0.2)
    turnON_ALL_LEDs()
    time.sleep(0.2)
    turnOFF_ALL_LEDs()
    time.sleep(0.2)
    turnON_ALL_LEDs()
    time.sleep(0.2)
    turnOFF_ALL_LEDs()
    time.sleep(0.5)

    # start vmu poller
    vmu_poller().start()
    time.sleep(0.5)

    # start gps poller
    gps_poller().start()

    # let things settle down for a spell before doing the good stuff
    time.sleep(0.5) 

    #start video recorder
    video_recorder().start()
    time.sleep(0.5)

    # enable logging
    logFlag = True
    current_log = newLog(logFileCount)
    print(current_log, 'open for logging')
    turnON_LED(Accel_Alarm)
    time.sleep(0.1)
    turnOFF_LED(Accel_Alarm)
    time.sleep(0.1)

    while True: 
        if logFlag == True:
            line = [dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), position['time'], position['lat'], position['lon'], yaw, pitch, roll, heading, AccX, AccY, AccZ]
            writeLine(current_log, line)
            if heading >= 300:
                alarm()
            turnON_LED(Spare_LED)
            time.sleep(0.1)
            turnOFF_LED(Spare_LED)
            time.sleep(0.1)


# Python bit to figure out how who started what
if __name__ == "__main__":
    main()
